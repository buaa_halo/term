# encoding: utf-8
from sys import argv

import matplotlib
import pymysql
from pyecharts.charts import Bar, Pie
from pyecharts import options as opts
# 内置主题类型可查看 pyecharts.globals.ThemeType
from pyecharts.globals import ThemeType


def paint(var, addr):
    matplotlib.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体 SimHei为黑体#连接数据库

    //改成自己的数据库
    db = pymysql.connect(host='localhost', user='root', password='root', database='demo', port=3306)


    # 使用cursor()方法创建一个游标对象
    cursor = db.cursor()
    sql = "select distinct (analysis_question_id) from analysis where analysis_questionnaire_id=%s" % var
    cursor.execute(sql)
    # 获取所有记录列表
    result = cursor.fetchall()

    sql = "select distinct (analysis_option) from analysis where analysis_questionnaire_id=%s" % var
    cursor.execute(sql)
    result_option = cursor.fetchall()

    sql = "select min(analysis_question_id)  from analysis where analysis_questionnaire_id=%s" % var
    cursor.execute(sql)
    min_question_id = cursor.fetchall()
    bar = Bar(init_opts=opts.InitOpts(theme=ThemeType.LIGHT)).set_global_opts(
        title_opts=opts.TitleOpts(title="问卷" + str(var)))

    x_data = []
    for row in result_option:
        x_data.append("选项" + str(row[0]+1))
    bar.add_xaxis(x_data)
    for row in result:
        y_data = []
        print(row[0])
        sql_question = "select * from analysis where analysis_question_id=%s" % row[0]
        cursor.execute(sql_question)
        res = cursor.fetchall()
        print(res)
        for row0 in res:
            analysis_number = row0[3]
            y_data.append(analysis_number)
        bar.add_yaxis("问题" + str(row[0] - min_question_id[0][0] + 1), y_data)

    bar.render(addr)
    # 关闭游标和数据库的连接
    cursor.close()
    db.close()


if __name__ == '__main__':
    paint(argv[1], argv[2])
