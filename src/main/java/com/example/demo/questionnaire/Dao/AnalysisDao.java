package com.example.demo.questionnaire.Dao;

import com.example.demo.questionnaire.Entity.Analysis;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AnalysisDao {
    void insertAnalysis(Analysis analysis);
    List<Analysis> selectAnalysis(Integer questionnaire_id, Integer question_id);
    void updateAnalysis(Analysis analysis);
}
