package com.example.demo.questionnaire.Dao;

import com.example.demo.questionnaire.Entity.Answers;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;

@Mapper
public interface AnswerDao {
    void addAnswers(Answers answers);
    Answers getAnswers(Integer question_id, Integer questionnaire_id,Integer user_id);
    ArrayList<Answers> getAnswersListByQuestion(Integer question_id);
    ArrayList<Answers> getAnswersByQuestionnaireAndUser(Integer questionnaire_id,Integer user_id);
    void deleteAnswersByQuestionnaireAndUser(Integer questionnaire_id,Integer user_id);
    void deleteAnswersByQuestionnaire(Integer questionaire_id);
}
