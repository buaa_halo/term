package com.example.demo.questionnaire.Dao;

import com.example.demo.questionnaire.Entity.Questions;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface QuestionDao {
    public Boolean insertQuestions(Questions questions);
    public List<Questions> selectQuestions(Integer questionnaireId);
    public Boolean deleteQuestions(Integer  questionnaireId);
    public Questions selectQuestionByQuestionId(Integer questionId);
}
