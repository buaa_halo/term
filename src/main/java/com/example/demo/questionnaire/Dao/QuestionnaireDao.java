package com.example.demo.questionnaire.Dao;

import com.example.demo.questionnaire.Entity.Questionnaires;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface QuestionnaireDao {

    public void createNewQuestionnaires(Questionnaires questionnaires);
    public Questionnaires selectQuestionnaires(Integer questionnaires_id);
    public List<Questionnaires> selectQuestionnairesByStr(String str);
    public void releaseQuestionnaires(Integer questionnaires_id);
    public void cancelQuestionnaires(Integer questionnaires_id);
    public void deleteQuestionnaires(Integer questionnaires_id);
    public void restoreQuestionnaires(Integer questionnaires_id);
    public void removeQuestionnaires(Integer questionnaires_id);
    public void updateQuestionnaires(Integer questionnaires_id, String questionnaires_title, String questionnaires_description, Date questionnaires_start_time, Date questionnaires_end_time);
    public void updateQuestionnairesAnswerCount(Integer questionnaires_answer_count,Integer questionnaires_id);

}
