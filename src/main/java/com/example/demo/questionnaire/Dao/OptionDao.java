package com.example.demo.questionnaire.Dao;

import com.example.demo.questionnaire.Entity.Options;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OptionDao {
public Boolean insertOptions(Options options);
public List<Options> selectOptions(Integer questionId);
public Boolean deleteOptions(Integer questionnaireId);
}
