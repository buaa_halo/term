package com.example.demo.questionnaire.Dao;


import com.example.demo.questionnaire.Entity.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    Users selectUserByUserNickName(String user_nick_name);
    void registerUser(Users user);
    void editUserPassword(String user_nick_name,String user_new_password);
    void updateUserInformation(Integer user_id ,String user_nick_name,String user_new_password,String user_email);
}
