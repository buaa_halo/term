package com.example.demo.questionnaire.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Questions {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer questions_id;
    private Integer questions_questionnaire_id;
    private Integer questions_order;
    private String questions_content;
    private String questions_describe;
    private Boolean questions_notnull;
    private Boolean questions_have_answers;
    private Integer questions_count;
    private String questions_type;
    public Questions(){
    }

    public Questions(Integer questions_questionnaire_id, Integer questions_order, String questions_content, String questions_describe, Boolean questions_notnull, Boolean questions_have_answers, Integer questions_count, String questions_type) {
        this.questions_questionnaire_id = questions_questionnaire_id;
        this.questions_order = questions_order;
        this.questions_content = questions_content;
        this.questions_describe = questions_describe;
        this.questions_notnull = questions_notnull;
        this.questions_have_answers = questions_have_answers;
        this.questions_count = questions_count;
        this.questions_type = questions_type;
    }

    public Integer getQuestions_id() {
        return questions_id;
    }

    public void setQuestions_id(Integer questions_id) {
        this.questions_id = questions_id;
    }

    public Integer getQuestions_questionnaire_id() {
        return questions_questionnaire_id;
    }

    public void setQuestions_questionnaire_id(Integer questions_questionnaire_id) {
        this.questions_questionnaire_id = questions_questionnaire_id;
    }

    public Integer getQuestions_order() {
        return questions_order;
    }

    public void setQuestions_order(Integer questions_order) {
        this.questions_order = questions_order;
    }

    public String getQuestions_content() {
        return questions_content;
    }

    public void setQuestions_content(String questions_content) {
        this.questions_content = questions_content;
    }

    public String getQuestions_describe() {
        return questions_describe;
    }

    public void setQuestions_describe(String questions_describe) {
        this.questions_describe = questions_describe;
    }

    public Boolean getQuestions_notnull() {
        return questions_notnull;
    }

    public void setQuestions_notnull(Boolean questions_notnull) {
        this.questions_notnull = questions_notnull;
    }

    public Boolean getQuestions_have_answers() {
        return questions_have_answers;
    }

    public void setQuestions_have_answers(Boolean questions_have_answers) {
        this.questions_have_answers = questions_have_answers;
    }

    public Integer getQuestions_count() {
        return questions_count;
    }

    public void setQuestions_count(Integer questions_count) {
        this.questions_count = questions_count;
    }

    public String getQuestions_type() {
        return questions_type;
    }

    public void setQuestions_type(String questions_type) {
        this.questions_type = questions_type;
    }

    @Override
    public String toString() {
        return "Questions{" +
                "questions_id=" + questions_id +
                ", questions_questionnaire_id=" + questions_questionnaire_id +
                ", questions_order=" + questions_order +
                ", questions_content='" + questions_content + '\'' +
                ", questions_describe='" + questions_describe + '\'' +
                ", questions_notnull=" + questions_notnull +
                ", questions_have_answers=" + questions_have_answers +
                ", questions_count=" + questions_count +
                ", questions_type=" + questions_type +
                '}';
    }
}