package com.example.demo.questionnaire.Entity;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Questionnaires {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer questionnaires_id;

    private Integer questionnaires_uid;

    private Date questionnaires_create_time;

    private Date questionnaires_start_time;

    private Date questionnaires_end_time;

    private Integer questionnaires_state;

    private Integer questionnaires_type;

    private String questionnaires_title;

    private Integer questionnaires_answer_count;

    private String questionnaires_description;

    public Questionnaires(Integer questionnaires_uid, Date questionnaires_create_time, Date questionnaires_start_time, Date questionnaires_end_time, Integer questionnaires_state, Integer questionnaires_type, String questionnaires_title, String questionnaires_description,Integer questionnaires_answer_count) {
        this.questionnaires_uid = questionnaires_uid;
        this.questionnaires_create_time = questionnaires_create_time;
        this.questionnaires_start_time = questionnaires_start_time;
        this.questionnaires_end_time = questionnaires_end_time;
        this.questionnaires_state = questionnaires_state;
        this.questionnaires_type = questionnaires_type;
        this.questionnaires_title = questionnaires_title;
        this.questionnaires_description = questionnaires_description;
        this.questionnaires_answer_count = questionnaires_answer_count;
    }

    public Questionnaires() {

    }

    public Integer getQuestionnaires_answer_count() {
        return questionnaires_answer_count;
    }

    public void setQuestionnaires_answer_count(Integer questionnaires_answer_count) {
        this.questionnaires_answer_count = questionnaires_answer_count;
    }

    @Override
    public String toString() {
        return "Questionnaires{" +
                "questionnaires_id=" + questionnaires_id +
                ", questionnaires_uid=" + questionnaires_uid +
                ", questionnaires_create_time=" + questionnaires_create_time +
                ", questionnaires_start_time=" + questionnaires_start_time +
                ", questionnaires_end_time=" + questionnaires_end_time +
                ", questionnaires_state=" + questionnaires_state +
                ", questionnaires_type=" + questionnaires_type +
                ", questionnaires_title='" + questionnaires_title + '\'' +
                ", questionnaires_description='" + questionnaires_description + '\'' +
                '}';
    }

    public Integer getQuestionnaire_id() {
        return questionnaires_id;
    }

    public void setQuestionnaire_id(Integer questionnaires_id) {
        this.questionnaires_id = questionnaires_id;
    }

    public Integer getQuestionnaire_uid() {
        return questionnaires_uid;
    }

    public void setQuestionnaire_uid(Integer questionnaires_uid) {
        this.questionnaires_uid = questionnaires_uid;
    }

    public Date getQuestionnaire_create_time() {
        return questionnaires_create_time;
    }

    public void setQuestionnaire_create_time(Date questionnaires_create_time) {
        this.questionnaires_create_time = questionnaires_create_time;
    }

    public Date getQuestionnaire_start_time() {
        return questionnaires_start_time;
    }

    public void setQuestionnaire_start_time(Date questionnaires_start_time) {
        this.questionnaires_start_time = questionnaires_start_time;
    }

    public Date getQuestionnaire_end_time() {
        return questionnaires_end_time;
    }

    public void setQuestionnaire_end_time(Date questionnaires_end_time) {
        this.questionnaires_end_time = questionnaires_end_time;
    }

    public Integer getQuestionnaire_state() {
        return questionnaires_state;
    }

    public void setQuestionnaire_state(Integer questionnaires_state) {
        this.questionnaires_state = questionnaires_state;
    }

    public Integer getQuestionnaire_type() {
        return questionnaires_type;
    }

    public void setQuestionnaire_type(Integer questionnaires_type) {
        this.questionnaires_type = questionnaires_type;
    }

    public String getQuestionnaire_title() {
        return questionnaires_title;
    }

    public void setQuestionnaire_title(String questionnaires_title) {
        this.questionnaires_title = questionnaires_title;
    }

    public String getQuestionnaire_description() {
        return questionnaires_description;
    }

    public void setQuestionnaire_description(String questionnaires_description) {
        this.questionnaires_description = questionnaires_description;
    }
}
