package com.example.demo.questionnaire.Entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Options {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer options_id;
    private Integer options_questionnaire_id;
    private Integer options_question_id;
    private String  options_content;
    private Integer options_value;
    private Integer options_order;
    private Integer options_number;

    public Options(Integer options_questionnaire_id, Integer options_question_id, String options_content, Integer options_value, Integer options_order, Integer options_number) {
        this.options_questionnaire_id = options_questionnaire_id;
        this.options_question_id = options_question_id;
        this.options_content = options_content;
        this.options_value = options_value;
        this.options_order = options_order;
        this.options_number = options_number;
    }

    public Options() {
    }

    public Integer getOptions_id() {
        return options_id;
    }

    public void setOptions_id(Integer options_id) {
        this.options_id = options_id;
    }

    public Integer getOptions_questionnaire_id() {
        return options_questionnaire_id;
    }

    public void setOptions_questionnaire_id(Integer options_questionnaire_id) {
        this.options_questionnaire_id = options_questionnaire_id;
    }

    public Integer getOptions_question_id() {
        return options_question_id;
    }

    public void setOptions_question_id(Integer options_question_id) {
        this.options_question_id = options_question_id;
    }

    public String getOptions_content() {
        return options_content;
    }

    public void setOptions_content(String options_content) {
        this.options_content = options_content;
    }

    public Integer getOptions_value() {
        return options_value;
    }

    public void setOptions_value(Integer options_value) {
        this.options_value = options_value;
    }

    public Integer getOptions_order() {
        return options_order;
    }

    public void setOptions_order(Integer options_order) {
        this.options_order = options_order;
    }

    public Integer getOptions_number() {
        return options_number;
    }

    public void setOptions_number(Integer options_number) {
        this.options_number = options_number;
    }

    @Override
    public String toString() {
        return "Options{" +
                "options_id=" + options_id +
                ", options_questionnaire_id=" + options_questionnaire_id +
                ", options_question_id=" + options_question_id +
                ", options_content='" + options_content + '\'' +
                ", options_value=" + options_value +
                ", options_order=" + options_order +
                ", options_number=" + options_number +
                '}';
    }
}
