package com.example.demo.questionnaire.Entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Answers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer answers_id;
    private Integer answers_question_id;
    private Integer answers_questionnaire_id;
    private Integer answers_user_id;
    private String answer;
    public  Answers(){

    }

    public Answers(Integer answers_question_id, Integer answers_questionnaire_id, Integer answers_user_id, String answer) {
        this.answers_question_id = answers_question_id;
        this.answers_questionnaire_id = answers_questionnaire_id;
        this.answers_user_id = answers_user_id;
        this.answer = answer;
    }

    public Integer getAnswers_id() {
        return answers_id;
    }

    public void setAnswers_id(Integer answers_id) {
        this.answers_id = answers_id;
    }

    public Integer getAnswers_question_id() {
        return answers_question_id;
    }

    public void setAnswers_question_id(Integer answers_question_id) {
        this.answers_question_id = answers_question_id;
    }

    public Integer getAnswers_questionnaire_id() {
        return answers_questionnaire_id;
    }

    public void setAnswers_questionnaire_id(Integer answers_questionnaire_id) {
        this.answers_questionnaire_id = answers_questionnaire_id;
    }

    public Integer getAnswers_user_id() {
        return answers_user_id;
    }

    public void setAnswers_user_id(Integer answers_user_id) {
        this.answers_user_id = answers_user_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "Answers{" +
                "answers_id=" + answers_id +
                ", answers_question_id=" + answers_question_id +
                ", answers_questionnaire_id=" + answers_questionnaire_id +
                ", answers_user_id=" + answers_user_id +
                ", answer='" + answer + '\'' +
                '}';
    }
}
