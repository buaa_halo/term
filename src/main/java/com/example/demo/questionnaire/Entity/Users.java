package com.example.demo.questionnaire.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer user_id;

    private String user_password;
    private String user_nick_name;
    private String user_email;

    public Users(){}
    public Users(String nick_name, String password,String user_phone){
        this.user_password = password;
        this.user_nick_name = nick_name;

        this.user_email = user_phone;
    }

    public Users(Integer user_id,String nick_name, String password,String user_phone){
        this.user_id = user_id;
        user_password = password;
        user_nick_name = nick_name;
        this.user_email = user_phone;
    }
    public Users(String nick_name, String password){

        user_password = password;
        user_nick_name = nick_name;

    }



    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }
}
