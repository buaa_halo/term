package com.example.demo.questionnaire.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Analysis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Analysis_id;

    private Integer Analysis_questionnaire_id;
    private Integer Analysis_question_id;
    private Integer Analysis_option;
    private Integer Analysis_number;

    public Analysis(Integer analysis_questionnaire_id, Integer analysis_question_id, Integer analysis_option, Integer analysis_number) {
        Analysis_questionnaire_id = analysis_questionnaire_id;
        Analysis_question_id = analysis_question_id;
        Analysis_option = analysis_option;
        Analysis_number = analysis_number;
    }

    public Analysis(){}

    public Integer getAnalysis_id() {
        return Analysis_id;
    }

    public void setAnalysis_id(Integer analysis_id) {
        Analysis_id = analysis_id;
    }

    public Integer getAnalysis_questionnaire_id() {
        return Analysis_questionnaire_id;
    }

    public void setAnalysis_questionnaire_id(Integer analysis_Questionnaire_id) {
        Analysis_questionnaire_id = analysis_Questionnaire_id;
    }

    public Integer getAnalysis_question_id() {
        return Analysis_question_id;
    }

    public void setAnalysis_question_id(Integer analysis_question_id) {
        Analysis_question_id = analysis_question_id;
    }

    public Integer getAnalysis_option() {
        return Analysis_option;
    }

    public void setAnalysis_option(Integer analysis_option) {
        Analysis_option = analysis_option;
    }

    public Integer getAnalysis_number() {
        return Analysis_number;
    }

    public void setAnalysis_number(Integer analysis_number) {
        Analysis_number = analysis_number;
    }
}
