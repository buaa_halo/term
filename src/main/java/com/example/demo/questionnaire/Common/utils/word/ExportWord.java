package com.example.demo.questionnaire.Common.utils.word;

import cn.afterturn.easypoi.word.WordExportUtil;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.springframework.util.Assert;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExportWord {
    /**
     * 导出word
     * <p>第一步生成替换后的word文件，只支持docx</p>
     * <p>第二步下载生成的文件</p>
     * <p>第三步删除生成的临时文件</p>
     * 模版变量中变量格式：{{foo}}
     * @param templatePath word模板地址
     * @param temDir 生成临时文件存放地址
     * @param fileName 文件名
     * @param params 替换的参数
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    public static void exportWord(String templatePath, String temDir, String fileName, List<Map<String, Object>> params, HttpServletRequest request, HttpServletResponse response) {
        Assert.notNull(templatePath,"模板路径不能为空");
        Assert.notNull(temDir,"临时文件路径不能为空");
        Assert.notNull(fileName,"导出文件名不能为空");
        Assert.isTrue(fileName.endsWith(".docx"),"word导出请使用docx格式");
        if (!temDir.endsWith("/")){
            temDir = temDir + File.separator;
        }
        File dir = new File(temDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            String userAgent = request.getHeader("user-agent").toLowerCase();
            if (userAgent.contains("msie") || userAgent.contains("like gecko")) {
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else {
                fileName = new String(fileName.getBytes("utf-8"), "ISO-8859-1");
            }

            ///////////////////////////////////////////////
            List<XWPFDocument> wordList = new ArrayList<>();
            int num = params.size();
            for(int i = 0; i < num; i++){
                Map<String,Object> remap = params.get(i);
                if(i == 0){
                    String questionnaire_title = (String) remap.get("questionnaire_title");
                    String questionnaire_description = (String) remap.get("questionnaire_description");
                    XWPFDocument doc = new XWPFDocument();
                    XWPFParagraph paragraph = doc.createParagraph();
                    paragraph.setFontAlignment(2);
                    XWPFRun xwpfRun = paragraph.createRun();
                    xwpfRun.setFontFamily("黑体");
                    xwpfRun.setFontSize(20);
                    xwpfRun.setText(questionnaire_title+"\r\n");
                    xwpfRun.addBreak();
                    wordList.add(doc);
                }
                else{
                    XWPFDocument doc = createTable(remap);
                    wordList.add(doc);
                }
            }

            XWPFDocument word = WordUtils.mergeWord(wordList);
            String tmpPath = temDir + fileName;
            FileOutputStream fos = new FileOutputStream(tmpPath);
            word.write(fos);
            // 设置强制下载不打开
            response.setContentType("application/force-download");
            // 设置文件名
            response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);
            OutputStream out = response.getOutputStream();
            word.write(out);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            delFileWord(temDir,fileName);//这一步看具体需求，要不要删
        }
    }
    /**
     * 删除零时生成的文件
     */
    public static void delFileWord(String filePath, String fileName){
        File file =new File(filePath+fileName);
        File file1 =new File(filePath);
        file.delete();
        file1.delete();
    }
    public static XWPFDocument createTable(Map<String,Object> map){
        XWPFDocument doc = new XWPFDocument();
        String question_content = (String) map.get("question_content");
        Integer question_order = (Integer) map.get("question_order");

        String question_type = (String) map.get("question_type");


        if(question_type.equals("single-selection")){
            String[] contents = (String[]) map.get("contents");
            int[] options = (int[]) map.get("options");
            int num = options.length - 1;

            XWPFTable xwpfTable = doc.createTable(num, 2);
            XWPFParagraph paragraph = doc.createParagraph();
            XWPFRun xwpfRun = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            xwpfRun.setText("(单选)"+question_order+"."+question_content);
            xwpfRun.setFontFamily("宋体");
            xwpfRun.setFontSize(14);
            paragraph.addRun(xwpfRun);
            int j = 0;//计数器，判断第几个选项
            for(XWPFTableRow xwpfTableRow:xwpfTable.getRows()){//向每个表格填充第一列为选项内容，第二列为数目
                for(int i = 0;i < xwpfTableRow.getTableCells().size();i++){
                    XWPFTableCell xwpfTableCell = xwpfTableRow.getCell(i);
                    if(i == 0){
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText((j+1)+". "+contents[j]);
                    }
                    else{
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText(String.valueOf(options[j]));
                    }
                }
                j++;
            }
        }
        else if(question_type.equals("multiple-selection")){
            String[] contents = (String[]) map.get("contents");
            int[] options = (int[]) map.get("options");
            int num = options.length - 1;
            XWPFTable xwpfTable = doc.createTable(num, 2);
            XWPFParagraph paragraph = doc.createParagraph();
            XWPFRun xwpfRun = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            xwpfRun.setText("(多选)"+question_order+"."+question_content);
            xwpfRun.setFontFamily("宋体");
            xwpfRun.setFontSize(14);
            paragraph.addRun(xwpfRun);

            int j = 0;//计数器，判断第几个选项
            for(XWPFTableRow xwpfTableRow:xwpfTable.getRows()){//向每个表格填充第一列为选项内容，第二列为数目
                for(int i = 0;i < xwpfTableRow.getTableCells().size();i++){
                    XWPFTableCell xwpfTableCell = xwpfTableRow.getCell(i);
                    if(i == 0){
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText((j+1)+". "+contents[j]);
                    }
                    else{
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText(String.valueOf(options[j]));
                    }
                }
                j++;
            }
        }
        else if(question_type.equals("score-single-selection")){
            int[] contents = (int[]) map.get("contents");
            int num = contents.length-1;
            XWPFTable xwpfTable = doc.createTable(num, 2);
            XWPFParagraph paragraph = doc.createParagraph();
            XWPFRun xwpfRun = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            xwpfRun.setText("(评分题)"+question_order+"."+question_content);
            xwpfRun.setFontFamily("宋体");
            xwpfRun.setFontSize(14);
            paragraph.addRun(xwpfRun);

            int j = 0;//计数器，判断第几个选项
            for(XWPFTableRow xwpfTableRow:xwpfTable.getRows()){//向每个表格填充第一列为选项内容，第二列为数目
                for(int i = 0;i < xwpfTableRow.getTableCells().size();i++){
                    XWPFTableCell xwpfTableCell = xwpfTableRow.getCell(i);
                    if(i == 0){
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText((j+1)+". "+j+"分评价");
                    }
                    else{
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText(String.valueOf(contents[i]));
                    }
                }
                j++;
            }
        }
        else if(question_type.equals("single-filling")){
            String[] contents = (String[]) map.get("contents");
            int num = contents.length-1;
            XWPFTable xwpfTable = doc.createTable(num, 1);
            XWPFParagraph paragraph = doc.createParagraph();
            XWPFRun xwpfRun = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            xwpfRun.setText("(填空)"+question_order+"."+question_content);
            xwpfRun.setFontFamily("宋体");
            xwpfRun.setFontSize(14);
            paragraph.addRun(xwpfRun);

            int j = 0;//计数器，判断第几个选项
            for(XWPFTableRow xwpfTableRow:xwpfTable.getRows()){//向每个表格填充第一列为选项内容，第二列为数目
                for(int i = 0;i < xwpfTableRow.getTableCells().size();i++) {
                    XWPFTableCell xwpfTableCell = xwpfTableRow.getCell(i);
                    if (i == 0) {
                        CTTcPr tcPr = xwpfTableCell.getCTTc().addNewTcPr();
                        CTTblWidth cellw = tcPr.addNewTcW();
                        cellw.setType(STTblWidth.DXA);
                        cellw.setW(new BigInteger(String.valueOf(5000)));
                        xwpfTableCell.setText(contents[j]);
                    }
                }
            }
        }

        return doc;
    }
}

