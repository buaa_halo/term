package com.example.demo.questionnaire.Common.utils;

import java.io.*;
import java.net.HttpURLConnection;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.*;
import java.net.URL;
import java.nio.charset.Charset;


public class AddressUtils {
    /**
     * 读取
     *
     * @param rd
     * @return
     * @throws IOException
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    /**
     * 创建链接
     *
     * @param url
     * @return
     * @throws IOException
     * @throws JSONException
     */
    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = JSONObject.parseObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
    public static String []getAddress(String ip) throws IOException{
        String ak = "kK7fy6o6NgfDcR3COm6sGELgT5k5er1h";
        JSONObject json = readJsonFromUrl("http://api.map.baidu.com/location/ip?ip="+ip+"&ak="+ak);
        JSONObject obj = (JSONObject) ((JSONObject) json.get("content")).get("address_detail");
        String province = obj.getString("province");
        System.out.println(province);
        System.out.println(obj);
        JSONObject obj2 = (JSONObject) json.get("content");
        String address = obj2.getString("address");
        System.out.println(address);
        String arr[] = new String[2];
        arr[0] = province ;
        arr[1] = obj2.getString("city");
        return arr;

    }
//    public static void main(String args[]){
//        try{
//            System.out.println(getAddress("https://github.com/"));
//        }catch(Exception e){
//            System.out.println("ha");
//        }
//
//    }

}