package com.example.demo.questionnaire.Controller;

import com.example.demo.questionnaire.Common.utils.AddressUtils;
import com.example.demo.questionnaire.Common.utils.IpUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LocationController {
    @PostMapping("/getlocation")
    public Map<String,Object> getLocation(HttpServletRequest request){
        try{
            Map map = new HashMap();
            String ip = IpUtils.getIp(request);
            map.put( "province",AddressUtils.getAddress(ip)[0]);
            map.put("city",AddressUtils.getAddress(ip)[1]);
            map.put("success",true);
            map.put("message","读取成功");
            return map;
        }catch(Exception e){
            Map map = new HashMap();
            map.put("success",false);
            map.put("message","读取失败");
            return map;
        }

    }
}
