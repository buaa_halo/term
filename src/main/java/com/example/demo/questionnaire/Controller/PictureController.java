package com.example.demo.questionnaire.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PictureController {
    @PostMapping("pic")
    public void startCharaChange(@RequestBody Map<String,Object> map) throws Exception {
        String str= (String) map.get("name");
        String path=System.getProperty("user.dir")+"\\questionnaires\\src\\main\\resources";
        String[] arg = new String[] { "python", path+"\\pic.py",str,path+"\\a.html"};
        Process pr=Runtime.getRuntime().exec(arg);
    }
}