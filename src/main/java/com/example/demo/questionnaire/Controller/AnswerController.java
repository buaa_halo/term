package com.example.demo.questionnaire.Controller;

import com.example.demo.questionnaire.Entity.Answers;
import com.example.demo.questionnaire.Service.AnswerService;
import com.example.demo.questionnaire.Service.OptionService;
import com.example.demo.questionnaire.Service.QuestionService;
import com.example.demo.questionnaire.Service.QuestionnaireService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/answer")
public class AnswerController {
    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private OptionService optionService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    HttpSession session;
    @PostMapping("/getUserAnswer")
    public Map<String,Object> getUserAnswer(@RequestBody Map<String,Object> remap){
        System.out.println("/getUserAnswers " +remap);
        Map map = new HashMap();
        try{
            map.put("message","读取成功");
            map.put("success",true);
            Integer uid = (Integer)Integer.parseInt(remap.get("uid").toString());
            Integer questionnaire_id  = (Integer)Integer.parseInt(remap.get("questionnaire_id").toString());
            List<Answers>  answersList = answerService.getAnswersByQuestionnaireAndUser(questionnaire_id,uid);
            for (Answers a:answersList
            ) {
                map.put(a.getAnswers_question_id().toString(),a.getAnswer());

            }

        }catch(Exception e){
            map.put("message","读取失败");
            map.put("success","false");
        }

        return map;
    }
    @PostMapping("/deleteQuestionnaireAnswers")
    public Map<String, Object> deleteQuestionnaireAnswers(@RequestBody Map<String, Object> remap){
        System.out.println("/deleteQuestionnaireAnswers"+remap);
        Map map = new HashMap();
        try{
            Integer questionnaire_id =  (Integer)Integer.parseInt(remap.get("questionnaire_id").toString());
            answerService.deleteAnswersByQuestionnaire(questionnaire_id);
            map.put("success",true);
            map.put("message","删除成功");
        }catch(Exception e){
            map.put("success",false);
            map.put("message","删除失败");

        }
        return map;

    }
    @PostMapping("/deleteUserAnswer")
    public Map<String, Object> deleteUserAnswer(@RequestBody Map<String, Object> remap){
        System.out.println("/deleteUserAnswer"+remap);
        Map map = new HashMap();
        try{
            Integer questionnaire_id =  (Integer)Integer.parseInt(remap.get("questionnaire_id").toString());
            Integer user_id =  (Integer)Integer.parseInt(remap.get("uid").toString());
            answerService.deleteAnswersByQuestionnaireAndUser(questionnaire_id,user_id);
            map.put("success",true);
            map.put("message","删除成功");
        }catch(Exception e){
            map.put("success",false);
            map.put("message","删除失败");

        }
        return map;
    }



}
