package com.example.demo.questionnaire.Controller;

import com.example.demo.questionnaire.Entity.*;
import com.example.demo.questionnaire.Service.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@RestController
@RequestMapping("/questionnaire")
public class QuestionnaireController {

    @Autowired
    private AnalysisService analysisService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private OptionService optionService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    HttpSession session;


    SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日 HH时mm分");

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //问卷的四个状态
    //创建、发布（默认开启）、开启、关闭、删除、恢复（默认关闭）
    /*
    *   创建：0，可修改所有信息，不可填写,发布者可搜索
    *   发布/开启：1，信息不可修改，可填写，可预览，普通用户可见
    *   关闭：2，仅可修改问卷信息，不可修改题目信息，不可填写，可预览，普通用户不可见
    *   删除：3，禁，仅发布者可见
     */
    @PostMapping("/create")//问卷创建后可多次修改，发布后无法修改题目
    public Map<String, Object> createNewQuestionnaire(@RequestBody Map<String, Object> keyMap) throws Exception {
        System.out.println("q/create"+keyMap);
        Map<String,Object> map = new HashMap<>();
        Map remap = (Map)keyMap.get("questionnaire");

//      创建问卷
        String title = (String) remap.get("title");
        String description = (String) remap.get("description");
        String start_time = (String) remap.get("start_time");
        String end_time = (String) remap.get("end_time");
        Integer type = (Integer) Integer.parseInt(remap.get("type").toString());
        Integer uid = (Integer)Integer.parseInt( remap.get("uid").toString());

        Integer question_count = (Integer)Integer.parseInt(remap.get("question_count").toString());
        Date createTime=new Date();

        Date startTime = null;
      //  startTime = formatter.parse(start_time);
        Date endTime = null;
        //endTime = formatter.parse(end_time);

        Integer state = 0;//默认创建时问卷未发布

        Questionnaires questionnaires =new Questionnaires(uid,createTime,startTime,endTime,state,type,title,description,(Integer)0);

        System.out.println(questionnaires.getQuestionnaire_create_time());
        questionnaireService.createNewQuestionnaires(questionnaires);
//
        Integer questionnaire_id= questionnaires.getQuestionnaire_id();

//        插入题目
        int i ,j;
        String q = "questions";
        String o = "options";
        List<Map> qu ;
        qu =     (List<Map>) remap.get(q);

        for(i=0;i<qu.size();i++){

            Map Question = qu.get(i);

            String question_content = (String)Question.get("question_content");
            String question_type = (String) Question.get("question_type");
            Boolean question_have_answers = (Boolean)Question.get("question_have_answers");
            Integer option_count;
            if(Question.get("option_count")==null){
                option_count = 0;
            }else{
                option_count =  (Integer) Integer.parseInt(Question.get("option_count").toString());
            }

            Boolean question_notnull = (Boolean) Question.get("question_notnull");
            String question_describe = (String) Question.get("question_describe");
            Questions tmp = new Questions(questionnaire_id,(Integer)i,question_content,question_describe,question_notnull,question_have_answers,option_count,question_type);
            questionService.insertQuestions(tmp);
            List<Map> or = (List<Map>) Question.get(o);
            Integer question_id = tmp.getQuestions_id();
         //   System.out.println(Question);
            for(j = 0;j<or.size();j++){
                Map Option = or.get(j);

                String options_content = (String)Option.get("options_content");
                Integer options_number = (Integer) Integer.parseInt(Option.get("options_number").toString());
                Integer options_value = (Integer) Integer.parseInt(Option.get("options_value").toString()) ;
                Options tmpOptions = new Options(questionnaire_id,question_id,options_content,options_value,(Integer) j,options_number);
                optionService.insertOptions(tmpOptions);
            }
        }

        initAnalysis(questionnaire_id);
        map.put("success",true);
        map.put("message","问卷创建成功");
        return map;
    }

    public void initAnalysis(Integer questionnaire_id){ //初始化问卷分析表
        List<Questions> questionsList = questionService.selectQuestions(questionnaire_id);
        Collections.sort(questionsList, new Comparator<Questions>() {
            @Override
            public int compare(Questions o1, Questions o2) {
                return o1.getQuestions_order()-o2.getQuestions_order();
            }
        });//按照order顺序从小到大排序
        for(Questions question:questionsList){
            if(question.getQuestions_type().equals("single-selection")||question.getQuestions_type().equals("multiple-selection")){
                Integer question_id = question.getQuestions_id();
                Integer option_num = question.getQuestions_count();
                List<Options> optionsList = optionService.selectOptions(question_id);
                for(Options option:optionsList){
                    Analysis analysis = new Analysis(questionnaire_id,question_id,option.getOptions_order(),0);
                    analysisService.insertAnalysis(analysis);
                }
            }
            //新增评分题
            else if(question.getQuestions_type().equals("score-single-selection")){
                Integer question_id = question.getQuestions_id();
                for(int i = 0; i <= 5; i++){
                    Analysis analysis = new Analysis(questionnaire_id,question_id,i,0);
                    analysisService.insertAnalysis(analysis);
                }
            }
        }
    }

    @PostMapping("/release")//发布后未开启
    public Map<String, Object> releaseQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("/release"+remap);
        Map<String, Object> map=new HashMap<>();
        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        if(questionnaires ==null){
            map.put("success",false);
            map.put("message","问卷不存在");
            return map;
        }
        if(questionnaires.getQuestionnaire_state()==0){
            questionnaireService.releaseQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷发布成功");
        }
        else if(questionnaires.getQuestionnaire_state()==1){
            map.put("success",false);
            map.put("message","当前问卷已开启");
        }
        else if(questionnaires.getQuestionnaire_state()==2){
            questionnaireService.releaseQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷开启成功");
        }
        else if(questionnaires.getQuestionnaire_state()==3){
            map.put("success",false);
            map.put("message","当前问卷已删除");
        }
        return map;
    }

    @PostMapping("/restore")
    public Map<String, Object> restoreQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("/restore"+remap);
        Map<String, Object> map=new HashMap<>();
        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        if(questionnaires ==null){
            map.put("success",false);
            map.put("message","问卷不存在");
            return map;
        }
        else if(questionnaires.getQuestionnaire_state()==3){
            questionnaireService.restoreQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷恢复成功");
        }
        return map;
    }
    @PostMapping("/submit")
    public Map<String,Object> submitQuestionnaire(@RequestBody Map<String,Object> keyMap){
        System.out.println("q/submit"+keyMap);
        Map remap = (Map)keyMap.get("remap");
        Integer question_count = (Integer)Integer.parseInt(remap.get("question_count").toString()) ;
        Integer answers_user_id =  (Integer)Integer.parseInt(remap.get("answers_users_id").toString());// (Integer) remap.get("answers_users_id");


        Integer answers_questionnaire_id = (Integer) Integer.parseInt(remap.get("answers_questionnaire_id").toString());
        Integer type = (Integer)Integer.parseInt(remap.get("questionnaire_type").toString());
        int i;
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(answers_questionnaire_id);
        questionnaireService.updateQuestionnairesAnswerCount(questionnaires.getQuestionnaires_answer_count()+1,answers_questionnaire_id);
        for(i=1;i<=question_count.intValue();i++){
            Map tmp = (Map) remap.get("answer"+i);
            String ans = (String)tmp.get("answer").toString();
            Integer qid = (Integer) Integer.parseInt(tmp.get("question_id").toString());
            Answers Ans = new Answers(qid,answers_questionnaire_id,answers_user_id,ans);
            answerService.addAnswers(Ans);
        }
        Map map = new HashMap();
        map.put("success",true);
        return map;
    }
    @PostMapping("/cancel")
    public Map<String, Object> cancelQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("q/cancel");
        Map<String, Object> map=new HashMap<>();
        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        if(questionnaires ==null){
            map.put("success",false);
            map.put("message","问卷不存在");
            return map;
        }
        if(questionnaires.getQuestionnaire_state()==0){
            map.put("success",false);
            map.put("message","当前问卷未发布");
        }
        else if(questionnaires.getQuestionnaire_state()==1){
            questionnaireService.cancelQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷关闭成功");
        }
        else if(questionnaires.getQuestionnaire_state()==2){
            map.put("success",false);
            map.put("message","当前问卷已关闭");
        }
        else if(questionnaires.getQuestionnaire_state()==3){
            map.put("success",false);
            map.put("message","当前问卷已删除");
        }
        return map;
    }

    @PostMapping("/delete")
    public Map<String, Object> deleteQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("q/delete"+remap);
        Map<String, Object> map=new HashMap<>();
        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        if(questionnaires ==null){
            map.put("success",false);
            map.put("message","问卷不存在");
            return map;
        }
        if(questionnaires.getQuestionnaire_state()==0){
            questionnaireService.deleteQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷删除成功");
        }
        else if(questionnaires.getQuestionnaire_state()==1){
            questionnaireService.deleteQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷删除成功");
        }
        else if(questionnaires.getQuestionnaire_state()==2){
            questionnaireService.deleteQuestionnaires(id);
            map.put("success",true);
            map.put("message","问卷删除成功");
        }
        else if(questionnaires.getQuestionnaire_state()==3){
            map.put("success",false);
            map.put("message","当前问卷已删除");
        }
        return map;
    }

    @PostMapping("/update")
    public Map<String, Object> updateQuestionnaire(@RequestBody Map<String, Object> remap) throws Exception {
        System.out.println("q/update"+remap);
        Map<String, Object> map=new HashMap<>();

        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());

        String title = (String) remap.get("title");

        String description = (String) remap.get("description");

        Date createTime = new Date();

        String start_time = (String) remap.get("start_time");

        Date startTime = null;

        startTime = formatter.parse(start_time);

        String end_time = (String) remap.get("end_time");

        Date endTime = null;

        endTime = formatter.parse(end_time);

        Integer question_count= (Integer) Integer.parseInt(remap.get("question_count").toString());
        Questionnaires oldQuestionnaires = questionnaireService.selectQuestionnaires(id);

        if(oldQuestionnaires ==null){
            map.put("success",false);
            map.put("message","问卷不存在");
            return map;
        }
        System.out.println(oldQuestionnaires);
        if(oldQuestionnaires.getQuestionnaire_state()>1){
            map.put("success",false);
            map.put("message","问卷不可修改，请在关闭/恢复问卷后进行修改");
            return map;
        }
        else if(oldQuestionnaires.getQuestionnaire_state()==1){
            questionnaireService.updateQuestionnaires(id,title,description,startTime,endTime);
            map.put("success",false);
            map.put("message","问卷已发布，仅对问卷部分信息可修改");
            return map;
        }
        else{
            questionnaireService.removeQuestionnaires(oldQuestionnaires.getQuestionnaire_id());
            questionService.deleteQuestions(oldQuestionnaires.getQuestionnaire_id());
            optionService.deleteOptions(oldQuestionnaires.getQuestionnaire_id());

            Questionnaires questionnaires =new Questionnaires(oldQuestionnaires.getQuestionnaire_uid(), oldQuestionnaires.getQuestionnaire_create_time(),startTime,endTime, oldQuestionnaires.getQuestionnaire_state(), oldQuestionnaires.getQuestionnaire_type(),title,description,oldQuestionnaires.getQuestionnaires_answer_count());
            questionnaireService.createNewQuestionnaires(questionnaires);
            Integer questionnaire_id= questionnaires.getQuestionnaire_id();

            int i ,j;
            String q = "question";
            String o = "option";
            for(i=1; i<= question_count; i++){
                Map Question = (Map) remap.get(q+i);
                System.out.println(Question);
                String question_content = (String)Question.get("question_content");
                System.out.println(question_content);
                String question_type = (String) Question.get("question_type");
                Boolean question_have_answers = (Boolean)Question.get("question_have_answers");
                Integer option_count =  (Integer)Integer.parseInt( Question.get("option_count").toString());
                Boolean question_notnull = (Boolean) Question.get("question_notnull");
                String question_describe = (String) Question.get("question_describe");
                String answer = null;
                Answers answers=null;

                Questions tmp = new Questions(questionnaire_id,(Integer)i,question_content,question_describe,question_notnull,question_have_answers,question_count,question_type);
                questionService.insertQuestions(tmp);
                if(question_have_answers){
                    answer= (String) Question.get("question_answer");
                    answers=new Answers(tmp.getQuestions_id(),tmp.getQuestions_questionnaire_id(),oldQuestionnaires.getQuestionnaire_uid(),answer);
                    answerService.addAnswers(answers);
                }

                Integer question_id = tmp.getQuestions_id();
                for(j = 1; j<= option_count; j++){
                    Map Option = (Map)  Question.get(o+i);
                    String options_content = (String)Option.get("options_content");
                    Integer options_number = (Integer)Integer.parseInt( Option.get("options_number").toString());
                    Integer options_value = (Integer)Integer.parseInt( Option.get("options_value").toString());
                    Options tmpOptions = new Options(questionnaire_id,question_id,options_content,options_value,(Integer) j,options_number);
                    optionService.insertOptions(tmpOptions);
                }
            }


            map.put("success",true);
            map.put("message","问卷修改成功");
            return map;
        }

    }

    @PostMapping("/select")
    public Map<String, Object> selectQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("q/select"+remap);
        Integer id= (Integer) Integer.parseInt(remap.get("id").toString()) ;
        Map<String,Object> questionnaire_map = new HashMap<>();
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        questionnaire_map.put("title",questionnaires.getQuestionnaire_title());
        questionnaire_map.put("description",questionnaires.getQuestionnaire_description());
        questionnaire_map.put("create_time",sdf.format(questionnaires.getQuestionnaire_create_time()));
        if(questionnaires.getQuestionnaire_start_time()!=null)
        questionnaire_map.put("start_time",sdf.format(questionnaires.getQuestionnaire_start_time()));
        if(questionnaires.getQuestionnaire_end_time()!=null)
        questionnaire_map.put("end_time",sdf.format(questionnaires.getQuestionnaire_end_time()));
        questionnaire_map.put("uid",questionnaires.getQuestionnaire_uid());

        List<Questions> questionsList = questionService.selectQuestions(id);

        int question_count = questionsList.size();
        questionnaire_map.put("question_count",question_count);
        Collections.sort(questionsList, new Comparator<Questions>() {
            @Override
            public int compare(Questions o1, Questions o2) {
                return o1.getQuestions_order()-o2.getQuestions_order();
            }
        });//按照order顺序从小到大排序

        //下面是问题层
        for(int i = 1;i < question_count+1; i++){
            Map<String,Object> question_map = new HashMap<>();
            Questions question = questionsList.get(i-1);
            question_map.put("question_content",question.getQuestions_content());
            question_map.put("question_type",question.getQuestions_type());
            question_map.put("question_have_answers",question.getQuestions_have_answers());
            question_map.put("question_notnull",question.getQuestions_notnull());
            question_map.put("question_describe",question.getQuestions_describe());
            //以下是选项层

            List<Options> optionsList = optionService.selectOptions(question.getQuestions_id());
            int option_count = optionsList.size();
            question_map.put("question_count",question_count);
            Collections.sort(optionsList, new Comparator<Options>() {
                @Override
                public int compare(Options o1, Options o2) {
                    return o1.getOptions_order()-o2.getOptions_order();
                }
            });//按照order顺序从小到大排序

            for (int j = 1;j < option_count+1;j++){
                Map<String,Object> option_map = new HashMap<>();
                Options option = optionsList.get(j-1);
                option_map.put("options_content",option.getOptions_content());
                option_map.put("options_value",option.getOptions_value());
                option_map.put("options_number",option.getOptions_number());

                question_map.put("option"+j,option_map);
            }

            questionnaire_map.put("question"+i,question_map);
        }
        return questionnaire_map;
    }


@PostMapping("/search")
public Map<String, Object> searchQuestionnaire(@RequestBody Map<String, Object> remap){
        System.out.println("q/search"+remap);
    Map<String, Object> map=new HashMap<>();
    Integer uid;
    if(remap.get("uid").toString()==null){
        uid = 3;
    }else{
        uid  = (Integer)Integer.parseInt(remap.get("uid").toString());
    }

    String str= (String) remap.get("str");
    Integer state = (Integer)Integer.parseInt(remap.get("state").toString());
    Integer questionnaire_type=(Integer) Integer.parseInt(remap.get("type").toString());
    Integer createTimeOrder= (Integer) Integer.parseInt(remap.get("createTimeOrder").toString());//0升序，1降序
    Integer startTimeOrder= (Integer) Integer.parseInt(remap.get("startTimeOrder").toString());//0升序，1降序
    Integer statusOrder=(Integer) Integer.parseInt(remap.get("statusOrder").toString());
    List<Questionnaires> list;
    list= questionnaireService.selectQuestionnairesByStr(str);
    System.out.println(list);
    if(uid!=-1)
        for(int i=0;i<list.size();i++){
            if(!list.get(i).getQuestionnaire_uid().equals(uid)){
                list.remove(i);
                i--;
            }
        }
    if(questionnaire_type!=-1)
        for(int i=0;i<list.size();i++){
            if(!list.get(i).getQuestionnaire_type().equals(questionnaire_type)){
                list.remove(i);
                i--;
            }
        }
    if(state!=-1)
        for(int i=0;i<list.size();i++){
            if(!list.get(i).getQuestionnaire_state().equals(state)){
                list.remove(i);
                i--;
            }
        }
    if(state==-1)
        for(int i=0;i<list.size();i++){
            if(list.get(i).getQuestionnaire_state().equals(3)){
                list.remove(i);
                i--;
            }
        }
    if(createTimeOrder!=-1)
        list.sort((o1, o2) -> o1.getQuestionnaire_create_time().before(o2.getQuestionnaire_create_time()) ? -1 + createTimeOrder : -createTimeOrder);
    if(startTimeOrder!=-1)
        list.sort((o1, o2) -> o1.getQuestionnaire_start_time().before(o2.getQuestionnaire_start_time()) ? -1 + startTimeOrder : -startTimeOrder);
//    if(statusOrder!=-1)
//        list.sort((o1, o2) -> o1.getQuestionnaire_create_time().before(o2.getQuestionnaire_start_time()) ? -1 + createTimeOrder : -createTimeOrder);
    map.put("res",list);
    return map;
}
    @PostMapping("/copy")
    public Map<String, Object> copyQuestionnaire(@RequestBody Map<String,Object> remap){
        Map<String, Object> map=new HashMap<>();

        Integer id= (Integer) Integer.parseInt(remap.get("id").toString());
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(id);
        questionnaires.setQuestionnaire_state(0);
        questionnaireService.createNewQuestionnaires(questionnaires);

        List<Questions> questionsList=questionService.selectQuestions(id);

        for(Questions questions:questionsList){
            questions.setQuestions_questionnaire_id(questionnaires.getQuestionnaire_id());
            List<Options> optionsList=optionService.selectOptions(questions.getQuestions_id());
            questionService.insertQuestions(questions);
            for(Options options:optionsList){
                options.setOptions_question_id(questions.getQuestions_id());
                options.setOptions_questionnaire_id(questionnaires.getQuestionnaire_id());
                optionService.insertOptions(options);
                System.out.println(options);
            }
        }


        return map;
    }

    @PostMapping("/remove")
    public Map<String,Object> deleteQuestion(@RequestBody Map<String,Object> remap){
        System.out.println("q/remove"+remap);
        Map<String,Object> map = new HashMap<>();
        Integer id = (Integer) Integer.parseInt(remap.get("id").toString());
        if(questionService.deleteQuestions(id)){
            if(optionService.deleteOptions(id)){
                map.put("success",true);
                map.put("message","问卷成功从回收站移除");
                questionnaireService.removeQuestionnaires(id);
            }
            else{
                map.put("success",false);
                map.put("message","问卷选项删除失败");
            }
        }
        else {
            map.put("success", false);
            map.put("message", "问卷题目删除失败");
        }
        return map;
    }

    @PostMapping("/get")
    //通过id生成问卷，只是一个可调用方法
    public Map<String,Object> madeQuestionnaireById(@RequestBody Map<String,Object> remap){

        System.out.println("/get"+remap);
        Integer id = (Integer)Integer.parseInt((String)remap.get("id").toString());
        Map<String,Object> questionnaire_map = new HashMap<>();
        Questionnaires questionnaire = questionnaireService.selectQuestionnaires(id);
        if(questionnaire==null){
            questionnaire_map.put("success",false);
            questionnaire_map.put("message","问卷不存在");
            return questionnaire_map;
        }

        questionnaire_map.put("success",true);
        questionnaire_map.put("message","获取成功");
        questionnaire_map.put("type",questionnaire.getQuestionnaire_type());
        questionnaire_map.put("title",questionnaire.getQuestionnaire_title());
        questionnaire_map.put("description",questionnaire.getQuestionnaire_description());
        questionnaire_map.put("create_time",sdf.format(questionnaire.getQuestionnaire_create_time()));
        if(questionnaire.getQuestionnaire_start_time()!=null)
        questionnaire_map.put("start_time",sdf.format(questionnaire.getQuestionnaire_start_time()));
        if(questionnaire.getQuestionnaire_end_time()!=null)
        questionnaire_map.put("end_time",sdf.format(questionnaire.getQuestionnaire_end_time()));
        questionnaire_map.put("uid",questionnaire.getQuestionnaire_uid());
        questionnaire_map.put("questionnaire_id",questionnaire.getQuestionnaire_id());

        List<Questions> questionsList = questionService.selectQuestions(id);
        List<Map> list1 = new ArrayList<>();
        int question_count = questionsList.size();
        questionnaire_map.put("question_count",question_count);
        Collections.sort(questionsList, new Comparator<Questions>() {
            @Override
            public int compare(Questions o1, Questions o2) {
                return o1.getQuestions_order()-o2.getQuestions_order();
            }
        });//按照order顺序从小到大排序

        //下面是问题层
        for(int i = 1;i < question_count+1; i++){
            Map<String,Object> question_map = new HashMap<>();
            Questions question = questionsList.get(i-1);
            question_map.put("question_content",question.getQuestions_content());
            question_map.put("question_type",question.getQuestions_type());
            question_map.put("question_have_answers",question.getQuestions_have_answers());
            question_map.put("question_notnull",question.getQuestions_notnull());
            question_map.put("question_describe",question.getQuestions_describe());
            question_map.put("question_id",question.getQuestions_id());
            //以下是选项层
            List<Options> optionsList = optionService.selectOptions(question.getQuestions_id());
            int option_count = optionsList.size();
            question_map.put("question_count",question_count);
            Collections.sort(optionsList, new Comparator<Options>() {
                @Override
                public int compare(Options o1, Options o2) {
                    return o1.getOptions_order()-o2.getOptions_order();
                }
            });//按照order顺序从小到大排序
            List<Map> list2 = new ArrayList<>();
            for (int j = 1;j < option_count+1;j++){
                Map<String,Object> option_map = new HashMap<>();

                Options option = optionsList.get(j-1);
                option_map.put("options_content",option.getOptions_content());
                option_map.put("options_value",option.getOptions_value());
                option_map.put("options_number",option.getOptions_number());
                list2.add(option_map);

            }
            question_map.put("options",list2);
            list1.add(question_map);


        }
        questionnaire_map.put("questions",list1);
       // System.out.println(questionnaire_map);
        return questionnaire_map;

    }





}
