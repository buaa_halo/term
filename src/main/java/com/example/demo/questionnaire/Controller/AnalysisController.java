package com.example.demo.questionnaire.Controller;

import com.example.demo.questionnaire.Dao.OptionDao;
import com.example.demo.questionnaire.Entity.*;

import com.example.demo.questionnaire.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class AnalysisController {
    @Autowired
    private QuestionnaireService questionnaireService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnalysisService analysisService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private OptionService optionService;

    public Map<String,Object> QuestionAnalysis(Questions question, Integer questionnaire_id){
        Map<String,Object> map = new HashMap<>();
        Integer question_id = question.getQuestions_id();
        Integer option_num = question.getQuestions_count();
        List<Options> optionList = optionService.selectOptions(question_id);

        String contents[] = new String[option_num];//每一个选项的内容

        for(Options option:optionList){
            contents[option.getOptions_order()] = option.getOptions_content();
        }
        int options[] = new int[option_num];//记录每个选项选了多少个，下标代表选项,值代表数目
        List<Answers> answersList = answerService.getAnswersListByQuestion(question_id);
        Integer answers_num = answersList.size();
        for(int i = 0;i < answersList.size(); i++){
            Answers answer = answersList.get(i);
            String answer_str = answer.getAnswer();
            String [] str = answer_str.split("[\\&]+");
            for(int j = 0;j < str.length; j++){
                int option = Integer.parseInt(str[j])-1;
                options[option]++;
            }
        }

        for(int i = 0;i < option_num; i++){
            Analysis analysis = new Analysis(questionnaire_id,question_id,i,options[i]);
            analysisService.updateAnalysis(analysis);
        }
        map.put("contents",contents);
        map.put("options",options);
        map.put("totalFilledNum",answers_num);
        return map;
    }

    ///新增评分题
    public Map<String,Object> ScoreQuestionAnalysis(Questions question,Integer questionnaire_id){
        Map<String,Object> map = new HashMap<>();
        Integer question_id = question.getQuestions_id();
        List<Answers> answersList = answerService.getAnswersListByQuestion(question_id);
        int scores[] = new int[6];
        Integer answer_num = answersList.size();
        for(Answers answer: answersList) {
            Integer score = Integer.valueOf(answer.getAnswer());
            scores[score]++;
        }
        for(int i = 0; i <= 5; i++){
            Analysis analysis = new Analysis(questionnaire_id,question_id,i,scores[i]);
            analysisService.updateAnalysis(analysis);
        }

        map.put("contents",scores);
        map.put("totalFilledNum",answer_num);
        return map;
    }
    ///

    @RequestMapping(value = "/questionnaire/analysis")
    public List<Map<String,Object>> dataAnalysis(@RequestBody Map<String,Object> remap){
        System.out.println("/questionnaire/analysis" + remap );
        Integer questionnaire_id = (Integer)Integer.parseInt(remap.get("id").toString());
        List<Map<String,Object>> analysisList = new ArrayList<>();
        List<Questions> questionsList = questionService.selectQuestions(questionnaire_id);
        Map<String,Object> map = new HashMap<>();
        Questionnaires questionnaires = questionnaireService.selectQuestionnaires(questionnaire_id);

        map.put("questionnaire_title",questionnaires.getQuestionnaire_title());
        map.put("questionnaires_description",questionnaires.getQuestionnaire_description());
        analysisList.add(map);//analysisList中第一Map存放数据为问卷的题目和描述

        Collections.sort(questionsList, new Comparator<Questions>() {
            @Override
            public int compare(Questions o1, Questions o2) {
                return o1.getQuestions_order()-o2.getQuestions_order();
            }
        });//按照order顺序从小到大排序

        for(Questions question:questionsList){ //将每一个问题的分析进行处理
            if(question.getQuestions_type().equals("single-selection")||question.getQuestions_type().equals("multiple-selection")){
                Map<String,Object> optionsAnalysis = QuestionAnalysis(question,questionnaire_id);
                optionsAnalysis.put("question_content",question.getQuestions_content());
                optionsAnalysis.put("question_order",question.getQuestions_order());
                optionsAnalysis.put("question_type",question.getQuestions_type());

                analysisList.add(optionsAnalysis);
            }
            //新增评分内容
            else if(question.getQuestions_type().equals("score-single-selection")){
                Map<String,Object> optionsAnalysis = ScoreQuestionAnalysis(question,questionnaire_id);
                optionsAnalysis.put("question_content",question.getQuestions_content());
                optionsAnalysis.put("question_order",question.getQuestions_order());
                optionsAnalysis.put("question_type",question.getQuestions_type());

                analysisList.add(optionsAnalysis);
            }
            else if(question.getQuestions_type().equals("single-filling")){
                Map<String,Object> optionsAnalysis = new HashMap<>();
                Integer question_id = question.getQuestions_id();
                List<Answers> answersList = answerService.getAnswersListByQuestion(question_id);
                int answer_num = answersList.size();
                int i = 0;
                String [] contents = new String[answer_num];
                for(Answers answer:answersList){
                    contents[i++] = answer.getAnswer();
                }
                optionsAnalysis.put("contents",contents);
                optionsAnalysis.put("question_content",question.getQuestions_content());
                optionsAnalysis.put("question_order",question.getQuestions_order());
                optionsAnalysis.put("question_type",question.getQuestions_type());
                analysisList.add(optionsAnalysis);
            }
        }
        System.out.println(analysisList);
        return analysisList;
    }
}
