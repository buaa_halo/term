package com.example.demo.questionnaire.Controller;

//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.example.demo.questionnaire.Entity.Users;
//import com.example.demo.questionnaire.Service.UserService;
//
//import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.apache.commons.mail.HtmlEmail;
//import javax.servlet.http.HttpSession;
//import javax.websocket.Session;
//import java.net.Authenticator;
//import java.net.PasswordAuthentication;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Random;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.questionnaire.Entity.Users;
import com.example.demo.questionnaire.Service.UserService;
import com.zhenzi.sms.ZhenziSmsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    public HttpSession session;

    @Autowired
    UserService userService;

    @RequestMapping("/sendSMS")
    @ResponseBody
    public Map<String,Object> sendSms(@RequestBody Map<String ,Object>remap) {
        Map<String ,Object>map=new HashMap<>();

        System.out.println("/sendSMS "+remap);
        try {
            String number = (String) remap.get("number");
            String verifyCode = String.valueOf(new Random().nextInt(899999) + 100000);
//发送短信


//            HtmlEmail send=new HtmlEmail();
//            send.setHostName("smtp.qq.com");
//            send.setSmtpPort(587);
//            send.setCharset("utf-8");
//            send.setSSL(true);
//// 接收者的Eamil
//            send.addTo( number);
//// 参数1：发送者的QQEamil，参数2：发送者显示名字
//            send.setFrom("761590969@qq.com", "验证码服务");
//// 参数1：发送者的QQEmail，参数2：第一步获取的授权码
//            send.setAuthentication("761590969@qq.com", "mcefclvsxozqbccf");
//// 邮件标题
//            send.setSubject("验证码");
//// 邮件内容
//            send.setMsg("你好！您正在注册问卷星球，您的验证码是"+verifyCode);
//            send.send();
            final Properties props=new Properties();
            props.put("mail.smtp.auth","true");
            props.put("mail.smtp.host","smtp.qq.com");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.port", "465");
            props.put("mail.user","761590969@qq.com");
            props.put("mail.password","mcefclvsxozqbccf");
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    // 用户名、密码
                    String userName = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new PasswordAuthentication(userName, password);
                }
            };
            Session mailSession = Session.getInstance(props, authenticator);
            MimeMessage message = new MimeMessage(mailSession);
            try {
                InternetAddress from =new InternetAddress("761590969@qq.com","问卷星球");
                message.setFrom(from);
                InternetAddress to =new InternetAddress(number);
                message.setRecipient(MimeMessage.RecipientType.TO, to);
                message.setSubject("验证码");
                message.setContent("你好！您正在注册通过邮箱"+number+"注册问卷星球，您的验证码是 "+verifyCode+"。如非您本人操作，请忽略", "text/html;charset=UTF-8");
                Transport.send(message);
            }
            catch (MessagingException e){
                System.out.println(e.getMessage());
            }


            map.put("success",true);
            map.put("message",verifyCode);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("success",false);
            map.put("message",null);
        }
        return map;
    }

    @RequestMapping("/register")
    @ResponseBody
    public Map<String ,Object> register(@RequestBody Map<String, Object> remap) {
        System.out.println("/register "+remap);
        String verify_code = (String) remap.get("verify_code").toString();
        String user_nick_name =(String)remap.get("user_nick_name");//获取前端返回的昵称
        String user_password = (String)remap.get("user_password");//获取前端返回的密码
        String user_confirm_password =(String) remap.get("user_confirm_password");
        String user_email = (String) remap.get("user_mail");
        System.out.println(user_email);
        Map<String,Object> map = new HashMap<>();
        Users user = null;
        user = userService.selectUserByUserNickName(user_nick_name);
        try{
         String right_verify_code  = remap.get("right_verify_code").toString();
            if(!right_verify_code.equals(verify_code)){
                map.put("success",false);
                map.put("message","验证码错误");
            }
            else if(user==null){
                if(!user_password.equals(user_confirm_password)){
                    map.put("success",false);
                    map.put("message","两次输入密码不正确");
                }
                else{
                    user = new Users(user_nick_name,user_password,user_email);
                    userService.registerUser(user);
                    map.put("success",true);
                    map.put("message","用户注册成功");
                }
            }
            else{
                map.put("success",false);
                map.put("massage","用户名已存在");
            }
        }catch(Exception e){
            e.printStackTrace();
            map.put("success", false);
            map.put("message", "用户注册失败！");
        }

        return map;
    }

    @PostMapping (value = "/login")
    public Map<String,Object> userLogin(@RequestBody Map<String,String> remap){
        System.out.println("/login "+remap);
        String user_nick_name = remap.get("user_nick_name");//获取前端返回的昵称
        String user_password = remap.get("user_password");//获取前端返回的密码
        System.out.println(user_nick_name);
        Map<String,Object> map = new HashMap<>();
        Users user = userService.selectUserByUserNickName(user_nick_name);
        try{
            if(user == null){
                map.put("success",false);
                map.put("message","用户不存在");
            }
            else{
                if(user_password.equals(user.getUser_password())){
                    map.put("success",true);
                    map.put("user_id",user.getUser_id());
                    map.put("user_nick_name",user.getUser_nick_name());
                    map.put("user_password",user.getUser_password());
                    map.put("user_mail",user.getUser_email());
                }
                else{
                    map.put("success",false);
                    map.put("message","密码错误");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("success", false);
            map.put("message", "用户登录失败！");
        }
        System.out.println(map);
        return map;
    }

    @PostMapping( "/edit")
    public Map<String,Object> editUserInformation(@RequestBody Map<String,Object> remap){
        System.out.println("/user/edit"+remap);
        Map<String,Object> map = new HashMap<>();
        String user_nick_name =(String)remap.get("user_nick_name");//获取前端返回的昵称
        String user_password = (String)remap.get("user_password");//获取前端返回的密码
        String user_email = (String) remap.get("user_email");
        Integer user_id = (Integer) Integer.parseInt(remap.get("user_id").toString());

        try{
            userService.updateUserInformation(user_id,user_nick_name,user_password,user_email);
            map.put("success",true);
            map.put("message","修改成功");
        }catch (Exception e){
            e.printStackTrace();
            map.put("message","修改失败");
            map.put("success",false);
        }


        return map;
    }


}
