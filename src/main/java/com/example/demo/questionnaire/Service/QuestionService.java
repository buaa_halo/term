package com.example.demo.questionnaire.Service;

import com.example.demo.questionnaire.Entity.Questions;

import java.util.List;

public interface QuestionService {
    public Boolean insertQuestions(Questions question);
    public List<Questions>  selectQuestions(Integer questionnaireId);
    public Boolean deleteQuestions(Integer questionnaireId);
    public Questions selectQuestionByQuestionId(Integer questionId);
}
