package com.example.demo.questionnaire.Service.IMP;


import com.example.demo.questionnaire.Dao.UserDao;
import com.example.demo.questionnaire.Entity.Users;
import com.example.demo.questionnaire.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceIMP implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public Users selectUserByUserNickName(String user_nick_name) {

        return userDao.selectUserByUserNickName(user_nick_name);
    }

    @Override
    public void registerUser(Users user) {
        userDao.registerUser(user);
    }

    @Override
    public void editUserPassword(String user_nick_name, String user_new_password) {
        userDao.editUserPassword(user_nick_name,user_new_password);
    }

    @Override
    public void updateUserInformation(Integer user_id,String user_nick_name,String user_new_password,String user_email) {
        userDao.updateUserInformation(user_id,user_nick_name,user_new_password,user_email);
    }
}
