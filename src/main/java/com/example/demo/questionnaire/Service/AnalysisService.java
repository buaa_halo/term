package com.example.demo.questionnaire.Service;

import com.example.demo.questionnaire.Entity.Analysis;

import java.util.List;

public interface AnalysisService {
    void insertAnalysis(Analysis analysis);
    List<Analysis> selectAnalysis(Integer questionnaire_id, Integer question_id);
    void updateAnalysis(Analysis analysis);
}
