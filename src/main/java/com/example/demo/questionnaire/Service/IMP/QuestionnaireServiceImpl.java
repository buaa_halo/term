package com.example.demo.questionnaire.Service.IMP;

import com.example.demo.questionnaire.Dao.QuestionnaireDao;
import com.example.demo.questionnaire.Entity.Questionnaires;
import com.example.demo.questionnaire.Service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class QuestionnaireServiceImpl implements QuestionnaireService {
    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Override
    public void createNewQuestionnaires(Questionnaires questionnaires)
    {
        questionnaireDao.createNewQuestionnaires(questionnaires);
    }

    @Override
    public Questionnaires selectQuestionnaires(Integer questionnaire_id) {
        return questionnaireDao.selectQuestionnaires(questionnaire_id);
    }

    @Override
    public List<Questionnaires> selectQuestionnairesByStr(String str) {
        return questionnaireDao.selectQuestionnairesByStr(str);
    }

    @Override
    public void releaseQuestionnaires(Integer questionnaire_id) {
        questionnaireDao.releaseQuestionnaires(questionnaire_id);
    }

    @Override
    public void cancelQuestionnaires(Integer questionnaire_id) {
        questionnaireDao.cancelQuestionnaires(questionnaire_id);
    }

    @Override
    public void deleteQuestionnaires(Integer questionnaire_id) {
        questionnaireDao.deleteQuestionnaires(questionnaire_id);
    }

    @Override
    public void removeQuestionnaires(Integer questionnaire_id) {
        questionnaireDao.removeQuestionnaires(questionnaire_id);
    }

    @Override
    public void restoreQuestionnaires(Integer questionnaires_id) {
        questionnaireDao.restoreQuestionnaires(questionnaires_id);
    }

    @Override
    public void updateQuestionnaires(Integer questionnaire_id, String questionnaire_title, String questionnaire_description, Date questionnaire_start_time, Date questionnaire_end_time) {
        questionnaireDao.updateQuestionnaires(questionnaire_id,questionnaire_title,questionnaire_description,questionnaire_start_time,questionnaire_end_time);
    }
    public void updateQuestionnairesAnswerCount(Integer questionnaires_answer_count,Integer questionnaires_id){
        questionnaireDao.updateQuestionnairesAnswerCount(questionnaires_answer_count,questionnaires_id);
    }
}
