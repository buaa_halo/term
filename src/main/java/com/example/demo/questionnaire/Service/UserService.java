package com.example.demo.questionnaire.Service;


import com.example.demo.questionnaire.Entity.Users;

public interface UserService {
    Users selectUserByUserNickName(String user_nick_name);
    void registerUser(Users user);
    void editUserPassword(String user_nick_name,String user_new_password);
    void updateUserInformation(Integer user_id,String user_nick_name,String user_new_password,String user_email);
}
