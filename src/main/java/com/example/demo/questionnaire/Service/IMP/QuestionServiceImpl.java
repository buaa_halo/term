package com.example.demo.questionnaire.Service.IMP;


import com.example.demo.questionnaire.Dao.QuestionDao;
import com.example.demo.questionnaire.Entity.Questions;
import com.example.demo.questionnaire.Service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionDao QuestionDao;
    @Override
    public Boolean insertQuestions(Questions question){
        QuestionDao.insertQuestions(question);
        return true;
    }
    public List<Questions> selectQuestions(Integer questionnaireId){
        return QuestionDao.selectQuestions(questionnaireId);
    }
    public Boolean deleteQuestions(Integer questionnaireId){
        QuestionDao.deleteQuestions(questionnaireId);
        return true;
    }

    @Override
    public Questions selectQuestionByQuestionId(Integer questionId) {
        return QuestionDao.selectQuestionByQuestionId(questionId);
    }
}
