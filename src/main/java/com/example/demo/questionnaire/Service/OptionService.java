package com.example.demo.questionnaire.Service;

import com.example.demo.questionnaire.Entity.Options;

import java.util.List;

public interface OptionService {
    public Boolean insertOptions(Options Option);
    public List<Options> selectOptions(Integer questionId);
    public Boolean deleteOptions(Integer questionnaireId);
}
