package com.example.demo.questionnaire.Service.IMP;

import com.example.demo.questionnaire.Dao.AnswerDao;
import com.example.demo.questionnaire.Entity.Answers;
import com.example.demo.questionnaire.Service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AnswerServerIMP implements AnswerService {
    @Autowired
    private AnswerDao answerDao;

    @Override
    public void addAnswers(Answers answers) {
        answerDao.addAnswers(answers);
    }

    @Override
    public Answers getAnswers(Integer question_id, Integer questionnaire_id, Integer user_id) {
        return answerDao.getAnswers(question_id,questionnaire_id,user_id);
    }

    @Override
    public ArrayList<Answers> getAnswersListByQuestion(Integer question_id) {
        return answerDao.getAnswersListByQuestion(question_id);
    }
    @Override
    public ArrayList<Answers> getAnswersByQuestionnaireAndUser(Integer questionnaire_id, Integer user_id){
        return answerDao.getAnswersByQuestionnaireAndUser(questionnaire_id,user_id);
    }
    public   void deleteAnswersByQuestionnaireAndUser(Integer questionnaire_id,Integer user_id){
        answerDao.deleteAnswersByQuestionnaireAndUser(questionnaire_id,user_id);
    }
    public  void deleteAnswersByQuestionnaire(Integer questionaire_id){
        answerDao.deleteAnswersByQuestionnaire(questionaire_id);
    }
}
