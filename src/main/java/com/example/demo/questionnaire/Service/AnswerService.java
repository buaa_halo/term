package com.example.demo.questionnaire.Service;


import com.example.demo.questionnaire.Entity.Answers;

import java.util.ArrayList;
import java.util.List;

public interface AnswerService {
    void addAnswers(Answers answers);
    Answers getAnswers(Integer question_id, Integer questionnaire_id,Integer user_id);
    List<Answers> getAnswersListByQuestion(Integer question_id);
   ArrayList<Answers> getAnswersByQuestionnaireAndUser(Integer questionnaire_id, Integer user_id);
    void deleteAnswersByQuestionnaireAndUser(Integer questionnaire_id,Integer user_id);
    void deleteAnswersByQuestionnaire(Integer questionaire_id);

}
