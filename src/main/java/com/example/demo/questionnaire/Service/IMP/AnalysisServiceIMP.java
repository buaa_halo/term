package com.example.demo.questionnaire.Service.IMP;

import com.example.demo.questionnaire.Dao.AnalysisDao;
import com.example.demo.questionnaire.Entity.Analysis;
import com.example.demo.questionnaire.Service.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnalysisServiceIMP implements AnalysisService {
    @Autowired
    private AnalysisDao analysisDao;
    @Override
    public void insertAnalysis(Analysis analysis) {
        analysisDao.insertAnalysis(analysis);
    }

    @Override
    public List<Analysis> selectAnalysis(Integer questionnaire_id, Integer question_id) {
        return analysisDao.selectAnalysis(questionnaire_id,question_id);
    }

    @Override
    public void updateAnalysis(Analysis analysis) {
        analysisDao.updateAnalysis(analysis);
    }

}
