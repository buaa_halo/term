package com.example.demo.questionnaire.Service.IMP;



import com.example.demo.questionnaire.Dao.OptionDao;
import com.example.demo.questionnaire.Entity.Options;
import com.example.demo.questionnaire.Service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OptionServiceImpl implements OptionService {
    @Autowired
    private OptionDao OptionDao;
    public Boolean insertOptions(Options options){
        OptionDao.insertOptions(options);
        return true;
    }
    public List<Options> selectOptions(Integer questionId){
        return OptionDao.selectOptions(questionId);
    }
    public Boolean deleteOptions(Integer questionnaireId){

         OptionDao.deleteOptions(questionnaireId);

        return true;
    }
}
